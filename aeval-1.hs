import qualified Data.Map as Map
import Data.Maybe
import Control.Monad.State
import Control.Applicative

type Env = Map.Map Char Int
type Error = String

data AExp = Var Char 
            | Value Int
            | Plus AExp AExp
            | Mul AExp AExp
         
aeval1 :: AExp -> State (Int,[String]) Int 
aeval1 (Value v) =  get >>= \(a,b)-> put ((a+1),("eval Value":b)) >> return v
aeval1 (Var k) = get >>= \(a,b) -> put ((a+1),(("eval Variable "++ [k]) :b)) >> (return . fromJust . Map.lookup k $ currEnv)
aeval1 (Plus ax1 ax2) = do
                         (a,b)<- get  
                         put ((a+1),("eval addition" :b))
                         val1 <- aeval1 ax1
                         val2 <- aeval1 ax2
                         return (val1 + val2)
aeval1 (Mul ax1 ax2) = do
                         (a,b)<- get
                         put ((a+1),("eval multiplication" :b))
                         val1 <- aeval1 ax1
                         val2 <- aeval1 ax2
                         return (val1 * val2)
                        
currEnv = Map.fromList[('X',2),('Y',3),('Z',0)]

{-
runState (aeval1 (Plus (Value 3) (Value 7))) (0,[])
(10,(3,["eval Value","eval Value","eval addition"]))

-}
aeval2 :: AExp -> State Int (Either Error Int)
aeval2 (Value v) = do
                    x <- get
                    put (x+1)
                    return (Right v)
aeval2 (Var k) = do
                x<- get
                put (x+1)
                case (Map.lookup k currEnv) of
                  Nothing -> return (Left ("Unbound Variable " ++ [k]))
                  Just a  -> return (Right a)
aeval2 (Plus ax1 ax2) = do
                       x<- get
                       put (x+1)
                       val1 <- aeval2 ax1
                       val2 <- aeval2 ax2
                       return (liftA2 (+) val1  val2) 
aeval (Mul ax1 ax2) = do
                      x<- get
                      put (x+1)
                      val1 <- aeval2 ax1
                      val2 <- aeval2 ax2
                      return (liftA2 (*) val1 val2)

{-
runState (aeval2 (Plus (Mul (Var 'Y') (Value 4)) (Var 'X'))) 0
(Right 14,5)
-}